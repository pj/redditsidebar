If the page you are viewing has a discussion on Reddit, this extension will display a small Reddit tab on the right hand side of the page. Click this to open a sidebar that will allow you to view the discussion, vote on comments and reply.

You'll need to disable the Reddit toolbar to use this, although the sidebar provides all the functionality of the toolbar, and works on pages that Reddit's built in toolbar doesn't work, like Twitter and StackOverflow.

At present it doesn't currently work on https sites for security reasons.

## Planned Features ##

- Options Page
- HTTPS support
- Support for Reddit toolbar
- Submit page when link isn't on Reddit
- Choose interface version.

Based on the Hacker News Sidebar: https://chrome.google.com/webstore/detail/hhedbplnihmkekhgmaoikgfbkjjaocnl